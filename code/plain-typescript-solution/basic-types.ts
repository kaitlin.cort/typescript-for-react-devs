const a = 1;
const b = "Hello";

function add(num1, num2) {
  return num1 + num2;
}

add(a, b);

// Array types

// Tuple
type Tuple = [number, string, boolean];

// Array
type ArrayOfNumbers = number[];
type ArrayOfStrings = string[];
type ArrayOfBooleans = boolean[];

// Object types
type Dog = {
  name: string;
  age: number;
  breed: string;
};

// Object with optional properties
type Cat = {
  name: string;
  age?: number;
  breed?: string;
};

// Using Partial with objects
type PartialDog = Partial<Dog>;

// Object with readonly properties
type ReadonlyDog = Readonly<Dog>;

// Setting one property to be readonly
type ReadonlyNameDog = {
  readonly name: string;
  age: number;
  breed: string;
};

// Union types
type NumberOrString = number | string;

// Union types with objects
type DogOrCat = Dog | Cat;

// Intersection types
type DogAndCat = Dog & Cat;
