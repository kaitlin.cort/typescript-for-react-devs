import { useState } from 'react'
import './App.css'
import z from 'zod';
import CharacterComponent from './CharacterComponent';

const Character = z.object({
  name: z.string(),
  height: z.string(),
  hair_color: z.string(),
  films: z.array(z.string())
})

export type Character = z.infer<typeof Character>;

async function getCharacterById(id: string): Promise<Character | Error> {
  const response = await fetch(`https://swapi.dev/api/people/${id}/`);
  if (!response.ok) {
    return Error("No Response from Server");
  }
  const data = await response.json();
  const result = Character.safeParse(data);
  if (!result.success) {
    return Error("Invalid Data from Server");
  }
  
  return result.data;
}

type CharacterForm = {
  characterId: { value: string }
} & EventTarget

function App() {
  const [error, setError] = useState<Error>(); 
  const [character, setCharacter] = useState<Character>();

  async function handleSubmit(e: React.SyntheticEvent) {
    e.preventDefault();
    const form: CharacterForm = e.target as CharacterForm;
    const character = await getCharacterById(form.characterId.value);
    if (character instanceof Error) {
      setError(character);
      return;
    }
    setCharacter(character);
  }
  
  
  return (
    <>
      <div id="error">{error?.message}</div>
      <form onSubmit={handleSubmit}>
        <input type="text" name="characterId" />
        <button type="submit">Fetch Character</button>
        {character &&
          <CharacterComponent character={character} />
        }
      </form>
    </>
  )
}

export default App
