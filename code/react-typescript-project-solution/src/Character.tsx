import { Character } from "./App"

export default function CharacterComponent({
    character,
}: {
    character: Character
}) {
    return (
        <>
            <h2>{character.name}</h2>
            <p>Height: {character.height}</p>
            <p>Hair color: {character.hair_color}</p>
        </>
    )
}
