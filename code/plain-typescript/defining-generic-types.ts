type LLNode<T> = {
  value: T;
  next: LLNode<T> | null;
};

const number_node: LLNode<number> = {
  value: 1,
  next: null,
};

const string_node: LLNode<string | number> = {
  value: "Hello",
  next: {
    value: 3,
    next: null,
  },
};

class LLNodeClass<T> {
  value: T;
  next: LLNodeClass<T> | null;

  constructor(value: T, next: LLNodeClass<T> | null) {
    this.value = value;
    this.next = next;
  }
}

const node2 = new LLNodeClass<number>(2, null);

type User = {
  name: string;
  age: number;
};

const user_node: LLNode<User> = {
  value: {
    name: "John",
    age: 30,
  },
  next: null,
};
