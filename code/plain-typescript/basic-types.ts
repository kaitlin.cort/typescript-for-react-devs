const a = 1;
const b = 2;

function add(num1: number, num2: number): number {
  if (num1 > 10) {
    return num1 + num2;
  } else {
    return num1 - num2;
  }
}

console.log(add(a, b));

// Array types

// Tuple
type Tuple = [number, string, boolean];

const tup: Tuple = [1, "2", true];

// Array
type ArrayOfNumbers = number[];
type ArrayOfStrings = string[];
type ArrayOfBooleans = boolean[];

const numbers: ArrayOfNumbers = [1, 2, 3, 4];

// Object types
type Dog = {
  name: string;
  age: number;
  breed: string;
};

interface DogInterface {
  name: string;
  age: number;
  breed: string;
}

// Object with optional properties
type Cat = {
  name: string;
  age?: number;
  breed?: string;
};

const my_cat: Cat = {
  name: "Fluffy",
};

// Using Partial with objects
type PartialDog = Partial<Dog>;

// Object with readonly properties
type ReadonlyDog = Readonly<Dog>;

// Setting one property to be readonly
type ReadonlyNameDog = {
  readonly name: string;
  age: number;
  breed: string;
};

// Union types
type NumberOrString = number | string;

// Union types with objects
type DogOrCat = Dog | Cat;

// Intersection types
type DogAndCat = Dog & Cat;

type DogWithExtraStuff = Dog & {
  subbreed: string;
};
