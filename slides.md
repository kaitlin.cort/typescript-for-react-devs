---
theme: ~/.local/share/glamour/catppuccin/themes/mocha.json
author: Galvanize
---

# TypeScript for React Devs

---

## What is TypeScript?

A superset of JavaScript.

Any JavaScript file is a valid but not necessarily correct TypeScript file.

Rename a `.js` file to `.ts`.

_And then make a tsconfig.json file, and fix all the type errors._

---

## Why?

TypeScript will save your life.

---

## Not Really, but...

TypeScript makes the code your write safe and less error prone _at runtime_ by
doing _compile time_ checks

---

## Static vs Dynamic Type Checking

- Javascript = Dynamic type checking at runtime, when it's not doing coercion.
- TypeScript = Static type checking at compile time, but with _inference_

---

## Type Inference

```typescript
const a = 1; // Inferred to be a number
const b = "Hello"; // Inferred to be a string

// Infers num1 and num2 to be of type "any"
function add(num1, num2) {
  return num1 + num2;
}

// No error, but a warning about "any"
add(a, b);
```

---

## Type Checking

```typescript
const a = 1; // Inferred to be a number
const b = "Hello"; // Inferred to be a string

// We specify the type for arguments, it infers the return type
function add(num1: number, num2: number) {
  return num1 + num2;
}

// Produces a compiler error, because we can't pass the string `b`
add(a, b);
```

---

## Structural Typing

TypeScript uses a structural type system. This means that as long as the
"shape" of a type matches an object, it will be valid.

```typescript
// Defining a custom type alias
type User = {
  first_name: string;
  last_name: string;
  email: string;
};

// The first argument has been declared as User
function print_user(user: User) {}

// This compiles because the object we passed in matches the shape
// of the user type.
print_user({
  first_name: "Mary",
  last_name: "Smith",
  email: "mary.smith@example.com",
});
```

---

Let's try it out!

---

## Generic Type Parameters

Sometimes you build a function or object and do not know what the type should be
ahead of time.

Instead of using the `any` type, you can pass in a type using the `<>` symbols.

```typescript
// You can pass a type parameter to the useState hook
const [users, setUser] = useState<User[]>([]);

// You can pass them to the built-in Map and Set objects
const mySet = new Set<number>();
```

---

## Defining your own Generic Type Parameters

You can define your own generic types, it's common practice to name the first type `T` and then `U` and `V`, however you can name them whatever you want.

```typescript
// A type for a linked list node
type LLNode<T> = {
  value: T;
  next: LLNode<T> | null;
};

// A type for a linked list node
// We could use a custom name instead of T
type LLNode<Value> = {
  value: Value;
  next: LLNode<Value> | null;
};
```

---

Let's try it out!

---

## Using TypeScript in React

```typescript
// Define the type of your state with a generic type param
const [posts, setPosts] = useState<Post[]>([]);

// Event handlers should use React's Synthetic Event type
function handleSubmit(e: React.SyntheticEvent) {}

// Make sure you are handling undefined and null
// correctly in your types.
const [user, setUser] = useState<User | null>(null);

// You should define a custom type for props
type UserComponentProps = {
  user: User;
  isLoggedIn: boolean;
};

function UserComponent({ user, isLoggedIn }: UserComponentProps) {
  // component here
}
```

---

Let's build a React app with TypeScript!
